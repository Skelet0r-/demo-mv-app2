package main;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

/**
 * Hello world!
 *
 */
public class App {

    public static String url = "https://si3.bcentral.cl/indicadoressiete/secure/Serie.aspx?gcode=UF&param=RABmAFYAWQB3AGYAaQBuAEkALQAzADUAbgBNAGgAaAAkADUAVwBQAC4AbQBYADAARwBOAGUAYwBjACMAQQBaAHAARgBhAGcAUABTAGUAYwBsAEMAMQA0AE0AawBLAF8AdQBDACQASABzAG0AXwA2AHQAawBvAFcAZwBKAEwAegBzAF8AbgBMAHIAYgBDAC4ARQA3AFUAVwB4AFIAWQBhAEEAOABkAHkAZwAxAEEARAA=";

    public static void main(String[] args) {
        extraerDatos();
        buscarDato();
    }

    public static void extraerDatos() {
        try {
            final Document document = Jsoup.connect(url).get();
            int cont = 2;
            String dato;
            System.out.println("Dia    Enero      Febrero         Marzo           Abril          Mayo              Junio           Julio           Agosto       Septiembre      Octubre         Noviembre         Diciembre");
            for (int i = 0; i < 31; i++) {
                if (cont <= 9) {
                    System.out.print(i + 1 + "    ");
                    dato = document.select("#gr_ctl0" + (cont) + "_Enero").text();
                    System.out.print(" " + dato + "\t  ");
                    dato = document.select("#gr_ctl0" + (cont) + "_Febrero").text();
                    System.out.print(dato + "\t");
                    dato = document.select("#gr_ctl0" + (cont) + "_Marzo").text();
                    System.out.print(dato + "\t");
                    dato = document.select("#gr_ctl0" + (cont) + "_Abril").text();
                    System.out.print(dato + "\t");
                    dato = document.select("#gr_ctl0" + (cont) + "_Mayo").text();
                    System.out.print(dato + "\t");
                    dato = document.select("#gr_ctl0" + (cont) + "_Junio").text();
                    System.out.print(dato + "\t");
                    dato = document.select("#gr_ctl0" + (cont) + "_Julio").text();
                    System.out.print(dato + "\t");
                    dato = document.select("#gr_ctl0" + (cont) + "_Agosto").text();
                    System.out.print(dato + "\t");
                    dato = document.select("#gr_ctl0" + (cont) + "_Septiembre").text();
                    System.out.print(dato + "\t");
                    dato = document.select("#gr_ctl0" + (cont) + "_Octubre").text();
                    System.out.print(dato + "\t");
                    dato = document.select("#gr_ctl0" + (cont) + "_Noviembre").text();
                    System.out.print(dato + "\t");
                    dato = document.select("#gr_ctl0" + (cont) + "_Diciembre").text();
                    System.out.print(dato + "\t");
                    System.out.println("");
                    cont++;
                } else {
                    System.out.print(i + 1 + "    ");
                    dato = document.select("#gr_ctl" + (cont) + "_Enero").text();
                    System.out.print(dato + "\t  ");
                    dato = document.select("#gr_ctl" + (cont) + "_Febrero").text();
                    System.out.print(dato + "\t");
                    dato = document.select("#gr_ctl" + (cont) + "_Marzo").text();
                    System.out.print(dato + "\t");
                    dato = document.select("#gr_ctl" + (cont) + "_Abril").text();
                    System.out.print(dato + "\t");
                    dato = document.select("#gr_ctl" + (cont) + "_Mayo").text();
                    System.out.print(dato + "\t");
                    dato = document.select("#gr_ctl" + (cont) + "_Junio").text();
                    System.out.print(dato + "\t");
                    dato = document.select("#gr_ctl" + (cont) + "_Julio").text();
                    System.out.print(dato + "\t");
                    dato = document.select("#gr_ctl" + (cont) + "_Agosto").text();
                    System.out.print(dato + "\t");
                    dato = document.select("#gr_ctl" + (cont) + "_Septiembre").text();
                    System.out.print(dato + "\t");
                    dato = document.select("#gr_ctl" + (cont) + "_Octubre").text();
                    System.out.print(dato + "\t");
                    dato = document.select("#gr_ctl" + (cont) + "_Noviembre").text();
                    System.out.print(dato + "\t");
                    dato = document.select("#gr_ctl" + (cont) + "_Diciembre").text();
                    System.out.print(dato + "\t");
                    System.out.println("");
                    cont++;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void buscarDato(){
        Scanner s = new Scanner(System.in);

        System.out.println("Por favor indique el nombre del mes que desea buscar el valor de la uf");
        System.out.println("DEBE SER INGRESADO EN EL SIGUIENTE FORMATO: Enero, es decir la primera letra en mayuscula");
        String nombreMes = s.next();
        System.out.println("El mes ingresado es: " + nombreMes);
        System.out.println("Por favor indique el d�a del mes de " + nombreMes + " que desea revisar el valor de la uf");
        int diaMes = s.nextInt();
        System.out.println("La fecha finalmente a revisar es el d�a " + diaMes + " del mes " + nombreMes);

        try {
            final Document document = Jsoup.connect(url).get();
            if (diaMes >= 1 && diaMes <= 9) {
                String dato = document.select("#gr_ctl0" + (diaMes+1) + "_" + nombreMes).text();
                System.out.println("En la fecha " + diaMes + " del mes " + nombreMes);
                System.out.println("El valor de la UF es de : " + dato + " pesos chilenos");
            } else {
                String dato = document.select("#gr_ctl" + (diaMes+1) + "_" + nombreMes).text();
                System.out.println("En la fecha " + diaMes + " del mes " + nombreMes);
                System.out.println("El valor de la UF es de : " + dato + " pesos chilenos");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
